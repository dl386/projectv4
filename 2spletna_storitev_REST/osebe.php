<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');
 
$headers = apache_request_headers();

if(isset($headers["X-API-Key"]))
		{
			$auth = false;
			
			$poizvedba="SELECT token FROM auth_tokens";
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			while ($vrstica=mysqli_fetch_assoc($rezultat))
			{
				if($vrstica["token"]==$headers["X-API-Key"])
				{
					$auth=true;
				}
			}
			if(!$auth)
			{
				http_response_code(404);
				die();
			}
		}
		else
		{
			http_response_code(401);
			die();
		}


switch($_SERVER["REQUEST_METHOD"])

{
	case 'GET':
		if(!empty($_GET["vzdevek"]) && !empty($_GET["vloga"]))
		{
			tip_prijave($_GET["vzdevek"]);
		}
		else if(!empty($_GET["vzdevek"]))
		{
			pridobi_osebo($_GET["vzdevek"]);
		}
		else
		{
			pridobi_vse_osebe();
		}
		break;
 
	case 'POST':
		dodaj_osebo();
		break;
 
	case 'PUT':
		if(!empty($_GET["vzdevek"]))
		{
			posodobi_osebo($_GET["vzdevek"]);
		}
		else
		{
			http_response_code(404);
		}
 
		break;

	case 'DELETE':
	
		if(!empty($_GET["vzdevek"]))
		{
			izbrisi_osebo($_GET["vzdevek"]);
		}
		else
		{
			http_response_code(404);
		}
		break;
 
	default:
		http_response_code(405);
		break;
}
 
mysqli_close($zbirka);

function pridobi_vse_osebe()
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba="SELECT vzdevek, ime, priimek, email, vloga FROM oseba";	
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);
	echo json_encode($odgovor);
}
 
function pridobi_osebo($vzdevek)
{
	global $zbirka;
	$vzdevek=mysqli_escape_string($zbirka, $vzdevek);
 
	$poizvedba="SELECT vzdevek, ime, priimek, email, vloga FROM oseba WHERE vzdevek='$vzdevek'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	if(mysqli_num_rows($rezultat)>0)
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);
		echo json_encode($odgovor);
	}
	else
	{
		http_response_code(404);
	}
}
 
function dodaj_osebo()
{
	global $zbirka, $DEBUG;
	
	$podatki = json_decode(file_get_contents("php://input"),true);
	
	if(isset($podatki["vzdevek"], $podatki["geslo"], $podatki["ime"], $podatki["priimek"], $podatki["email"], $podatki["vloga"]))
	{	
		$vzdevek = mysqli_escape_string($zbirka, $podatki["vzdevek"]);
		$geslo = hash("md5", mysqli_escape_string($zbirka, $podatki["vzdevek"].$podatki["geslo"]));
		$ime = mysqli_escape_string($zbirka, $podatki["ime"]);
		$priimek = mysqli_escape_string($zbirka, $podatki["priimek"]);
		$email = mysqli_escape_string($zbirka, $podatki["email"]);
		$vloga = mysqli_escape_string($zbirka, $podatki["vloga"]);
 
		if(!oseba_obstaja($vzdevek))
		{
			$poizvedba="INSERT INTO oseba (vzdevek, geslo, ime, priimek, email, vloga) VALUES ('$vzdevek', '$geslo', '$ime', '$priimek', '$email', '$vloga')";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(201);
				$odgovor = URL_vira($vzdevek);
				echo json_encode($odgovor);
			}
			else
			{
				http_response_code(500);
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(409);
			pripravi_odgovor_napaka("Oseba že obstaja!");
		}
	}
	else
	{
		http_response_code(400);
	}
}
 
function posodobi_osebo($vzdevek)
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
 
	if(oseba_obstaja($vzdevek))
	{
		if(isset($podatki["ime"], $podatki["priimek"], $podatki["email"], $podatki["vloga"]))
		{	
			$ime = mysqli_escape_string($zbirka, $podatki["ime"]);
			$priimek = mysqli_escape_string($zbirka, $podatki["priimek"]);
			$email = mysqli_escape_string($zbirka, $podatki["email"]);
			$vloga = mysqli_escape_string($zbirka, $podatki["vloga"]);
 
			$poizvedba="UPDATE oseba SET ime='$ime', priimek='$priimek', email='$email', vloga='$vloga' WHERE vzdevek='$vzdevek'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(400);
		}
	}
	else
	{
		http_response_code(404);
	}
}

function izbrisi_osebo($vzdevek) 
{
	global $zbirka, $DEBUG;
	
			$poizvedba="DELETE from oseba WHERE vzdevek='$vzdevek'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}

function tip_prijave($vzdevek)
{							 
	global $zbirka, $DEBUG;
 
			$poizvedba="SELECT vloga FROM oseba WHERE vzdevek='$vzdevek'";
			
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			if(mysqli_num_rows($rezultat)>0)
			{
				$odgovor=mysqli_fetch_assoc($rezultat);
	 
				http_response_code(200);
				echo json_encode($odgovor);
			}
			else
			{
				http_response_code(500);
				
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}
?>
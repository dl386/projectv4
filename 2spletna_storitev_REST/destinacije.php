<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');

$headers = apache_request_headers();

if(isset($headers["X-API-Key"]))
		{
			$auth = false;
			
			$poizvedba="SELECT token FROM auth_tokens";
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			while ($vrstica=mysqli_fetch_assoc($rezultat))
			{
				if($vrstica["token"]==$headers["X-API-Key"])
				{
					$auth=true;
				}
			}
			if(!$auth)
			{
				http_response_code(404);
				die();
			}
		}
		else
		{
			http_response_code(401);
			die();
		}

switch($_SERVER["REQUEST_METHOD"])
{
	case 'GET':
		if(!empty($_GET["IDdestinacije"]))
		{
			pridobi_destinacijo($_GET["IDdestinacije"]);									
		}
		else
		{
			pridobi_vse_destinacije();
		}
		break;
 
	default:
		http_response_code(405);
		break;
} 
 
mysqli_close($zbirka);

function pridobi_destinacijo($IDdestinacije)
{
	global $zbirka;
	
	$IDdestinacije=mysqli_escape_string($zbirka, $IDdestinacije);
 
	$poizvedba="Select ime_destinacije FROM destinacija";
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	if(mysqli_num_rows($rezultat)>0)
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);
		echo json_encode($odgovor);
	}
	else
	{
		http_response_code(404);
	}
}
 
function pridobi_vse_destinacije() 
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba= "SELECT * FROM destinacija";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);
	echo json_encode($odgovor);
}
<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');

$headers = apache_request_headers(); 

if(isset($headers["X-API-Key"]))
		{
			$auth = false;
			
			$poizvedba="SELECT token FROM auth_tokens";
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			while ($vrstica=mysqli_fetch_assoc($rezultat))
			{
				if($vrstica["token"]==$headers["X-API-Key"])
				{
					$auth=true;
				}
			}
			if(!$auth)
			{
				http_response_code(404);
				die();
			}
		}
		else
		{
			http_response_code(401);
			die();
		}

switch($_SERVER["REQUEST_METHOD"])
{
	case 'GET':
		if(!empty($_GET["vloga"]))
		{
			pridobi_tip_prijave($_GET["vloga"]);
		}
		else
		{
			pridobi_vse_tipe_prijav();
		}
		break;
 
	default:
		http_response_code(405);
		
		break;
}
 
mysqli_close($zbirka);

function pridobi_tip_prijave($vloga)
{
	global $zbirka;
	$vloga=mysqli_escape_string($zbirka, $vloga);
 
	$poizvedba="SELECT vloga, opis FROM tip_prijave WHERE vloga='$vloga'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	if(mysqli_num_rows($rezultat)>0)
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);
		echo json_encode($odgovor);
	}
	else
	{
		http_response_code(404);
	}
}
function pridobi_vse_tipe_prijav()
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba="SELECT * FROM tip_prijave";	
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);
	echo json_encode($odgovor);
}
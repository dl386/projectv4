<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');

$headers = apache_request_headers();

if(isset($headers["X-API-Key"]))
		{
			$auth = false;
			
			$poizvedba="SELECT token FROM auth_tokens";
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			while ($vrstica=mysqli_fetch_assoc($rezultat))
			{
				if($vrstica["token"]==$headers["X-API-Key"])
				{
					$auth=true;
				}
			}
			if(!$auth)
			{
				http_response_code(404);
				die();
			}
		}
		else
		{
			http_response_code(401);
			die();
		}

switch($_SERVER["REQUEST_METHOD"])
{
	case 'GET':
		if(!empty($_GET["vzdevek"]))
		{
			pridobi_rezervacijo_osebe($_GET["vzdevek"]);
		}
		else if(!empty($_GET["IDrezervacije"]))
		{
			pridobi_rezervacijo_osebe_ID($_GET["IDrezervacije"]);
		}
		else if(!empty($_GET["vzdevekVse"]))
		{
			pridobi_vse_rezervacije_vzdevek($_GET["vzdevekVse"]);
		}
		else
		{
			pridobi_vse_rezervacije();
		}
		break;
 
	case 'POST':				
	dodaj_rezervacijo();
	break;
	
	case 'PUT':
		if(!empty($_GET["IDrezervacije"]))
		{
			posodobi_rezervacijo($_GET["IDrezervacije"]);
		}
		else
		{
			http_response_code(404);
		}
		break;

	case 'DELETE':
		if(!empty($_GET["IDrezervacije"]))
		{
		izbrisi_rezervacijo($_GET["IDrezervacije"]);
		}
		else
		{
			http_response_code(404);
		}
		break;
		
	default:
		http_response_code(405);
		break;
}
 
 
mysqli_close($zbirka);
 
function pridobi_vse_rezervacije()
{
	global $zbirka;
	$odgovor=array();
	
	$poizvedba= "SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, rezervacija.IDpotovanja, rezervacija.casovni_zig, potovanje.agencija FROM rezervacija JOIN potovanje ON potovanje.IDpotovanja = rezervacija.IDpotovanja";
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
	
	http_response_code(200);
	echo json_encode($odgovor);
}

function pridobi_rezervacijo_osebe($vzdevek)
{
	global $zbirka;
	$odgovor=array();
	$vzdevek=mysqli_escape_string($zbirka, $vzdevek);
 
	$poizvedba="SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, potovanje.IDpotovanja, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.agencija FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija ON potovanje.IDpotovanja=rezervacija.IDpotovanja WHERE vzdevek='$vzdevek'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
		http_response_code(200);		//OK
		echo json_encode($odgovor);
}

function pridobi_vse_rezervacije_vzdevek($vzdevek)
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba= "SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, rezervacija.IDpotovanja, rezervacija.casovni_zig, potovanje.agencija FROM rezervacija JOIN potovanje ON potovanje.IDpotovanja = rezervacija.IDpotovanja WHERE vzdevek='$vzdevek'";
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
 
	http_response_code(200);
	echo json_encode($odgovor);
}

function pridobi_rezervacijo_osebe_ID($IDrezervacije)
{
	global $zbirka;
	
	$IDrezervacije=mysqli_escape_string($zbirka, $IDrezervacije);
 
	$poizvedba="SELECT rezervacija.IDrezervacije, rezervacija.vzdevek, potovanje.IDpotovanja, destinacija.ime_destinacije, rezervacija.casovni_zig, potovanje.agencija FROM potovanje JOIN destinacija ON potovanje.IDdestinacije=destinacija.IDdestinacije JOIN rezervacija ON potovanje.IDpotovanja=rezervacija.IDpotovanja WHERE IDrezervacije='$IDrezervacije'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	if(mysqli_num_rows($rezultat)>0)
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);
		echo json_encode($odgovor);
	}
	else
	{
		http_response_code(404);
	}
}

function dodaj_rezervacijo() 
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
	
	$vzdevek = mysqli_escape_string($zbirka, $podatki["vzdevek"]);
	$IDpotovanja = mysqli_escape_string($zbirka, $podatki["IDpotovanja"]);
 
	if(isset($podatki["vzdevek"], $podatki["IDpotovanja"]));
	{	
	$poizvedba="SELECT *from rezervacija WHERE vzdevek='$vzdevek' and IDpotovanja='$IDpotovanja'";
		
		$result = mysqli_query($zbirka, $poizvedba);

		if (mysqli_num_rows($result) > 0) {
			echo
			"rezervacija osebe že obstaja";
		} 
		else 
		{
			$poizvedba="INSERT INTO rezervacija (vzdevek, IDpotovanja) VALUES ('$vzdevek', '$IDpotovanja')";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(201);
				$odgovor = URL_vira($vzdevek);
				echo json_encode($odgovor);
			}
		}
	}
}
function posodobi_rezervacijo($IDrezervacije)
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
	
		if(isset($podatki["IDrezervacije"], $podatki["IDpotovanja"]))
		{	
			$IDrezervacije = mysqli_escape_string($zbirka, $podatki["IDrezervacije"]);
			$IDpotovanja = mysqli_escape_string($zbirka, $podatki["IDpotovanja"]);
			
			$poizvedba="UPDATE rezervacija SET IDpotovanja='$IDpotovanja' WHERE IDrezervacije='$IDrezervacije'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
		}
		else
		{
			http_response_code(409);
		}
}
function izbrisi_rezervacijo($IDrezervacije)
{
	global $zbirka, $DEBUG;
	
			$poizvedba="DELETE from rezervacija WHERE IDrezervacije='$IDrezervacije'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}		
?>
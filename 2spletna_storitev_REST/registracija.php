<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');

if ($_SERVER["REQUEST_METHOD"] == 'POST')
{
	dodaj_osebo();
}
else
{
	http_response_code(405);
}

function dodaj_osebo()
{
	global $zbirka, $DEBUG;
	
	$podatki = json_decode(file_get_contents("php://input"),true);
	
	if(isset($podatki["vzdevek"], $podatki["geslo"], $podatki["ime"], $podatki["priimek"], $podatki["email"], $podatki["vloga"]))
	{	
		$vzdevek = mysqli_escape_string($zbirka, $podatki["vzdevek"]);
		$geslo = hash("md5", mysqli_escape_string($zbirka, $podatki["vzdevek"].$podatki["geslo"]));
		$ime = mysqli_escape_string($zbirka, $podatki["ime"]);
		$priimek = mysqli_escape_string($zbirka, $podatki["priimek"]);
		$email = mysqli_escape_string($zbirka, $podatki["email"]);
		$vloga = mysqli_escape_string($zbirka, $podatki["vloga"]);
 
		if(!oseba_obstaja($vzdevek))
		{
			$poizvedba="INSERT INTO oseba (vzdevek, geslo, ime, priimek, email, vloga) VALUES ('$vzdevek', '$geslo', '$ime', '$priimek', '$email', '$vloga')";
			if(mysqli_query($zbirka, $poizvedba))
			{
				generiraj_token(hash("md5",$podatki["vzdevek"].$podatki["geslo"]));
			}
		}
	}
}

function generiraj_token($token)
	{
		global $zbirka, $DEBUG;
		
		$poizvedba="INSERT INTO auth_tokens (token) VALUES ('$token')";
		
		mysqli_query($zbirka, $poizvedba);
		
	}
?>
<?php
$DEBUG = true;
 
include("orodja.php");
 
$zbirka = dbConnect();
 
header('Content-Type: application/json');

$headers = apache_request_headers();

if(isset($headers["X-API-Key"]))
		{
			$auth = false;
			
			$poizvedba="SELECT token FROM auth_tokens";
			$rezultat=mysqli_query($zbirka, $poizvedba);
			
			while ($vrstica=mysqli_fetch_assoc($rezultat))
			{
				if($vrstica["token"]==$headers["X-API-Key"])
				{
					$auth=true;
				}
			}
			if(!$auth)
			{
				http_response_code(404);
				die();
			}
		}
		else
		{
			http_response_code(401);
			die();
		}

switch($_SERVER["REQUEST_METHOD"])
{
	case 'GET':
		if(!empty($_GET["IDpotovanja"]))
		{
			pridobi_potovanje($_GET["IDpotovanja"]);										
		}
		else
		{
			pridobi_vsa_potovanja();
		}
		break;
 
	case 'POST':		
		dodaj_potovanje();
		break;
		
	case 'PUT':
		if(!empty($_GET["IDpotovanja"]))
		{
			posodobi_potovanje($_GET["IDpotovanja"]);
		}
		else
		{
			http_response_code(404);
		}
		break;
 
	case 'DELETE': 
		if(!empty($_GET["IDpotovanja"]))
		{
		izbrisi_potovanje($_GET["IDpotovanja"]);
		}
		else
		{
			http_response_code(404);
		}
		break;
 
	default:
		http_response_code(405);
		break;
}

mysqli_close($zbirka);

function pridobi_vsa_potovanja() 
{
	global $zbirka;
	$odgovor=array();
 
	$poizvedba="SELECT potovanje.IDpotovanja, destinacija.ime_destinacije, potovanje.datum, potovanje.trajanje, potovanje.agencija, potovanje.cena, potovanje.opis_aranzmaja, potovanje.casovni_zig FROM potovanje JOIN destinacija ON destinacija.IDdestinacije=potovanje.IDdestinacije";	
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
 
	while($vrstica=mysqli_fetch_assoc($rezultat))
	{
		$odgovor[]=$vrstica;
	}
	http_response_code(200);
	echo json_encode($odgovor);
}

function pridobi_potovanje($IDpotovanja)
{
	global $zbirka;
	
	$IDpotovanja=mysqli_escape_string($zbirka, $IDpotovanja);
 
	$poizvedba="SELECT potovanje.IDpotovanja, potovanje.IDdestinacije, destinacija.ime_destinacije, potovanje.datum, potovanje.trajanje, potovanje.agencija, potovanje.cena, potovanje.opis_aranzmaja, potovanje.casovni_zig FROM potovanje JOIN destinacija ON destinacija.IDdestinacije=potovanje.IDdestinacije WHERE IDpotovanja='$IDpotovanja'";
 
	$rezultat=mysqli_query($zbirka, $poizvedba);
	
	if(mysqli_num_rows($rezultat)>0)
	{
		$odgovor=mysqli_fetch_assoc($rezultat);
 
		http_response_code(200);
		echo json_encode($odgovor);
	}
	else
	{
		http_response_code(404);
	}
}

function dodaj_potovanje()
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);
 
	if(isset($podatki["IDdestinacije"], $podatki["datum"], $podatki["trajanje"], $podatki["agencija"], $podatki["cena"],$podatki["opis_aranzmaja"]))
	{	
		$IDdestinacije = mysqli_escape_string($zbirka, $podatki["IDdestinacije"]);
		$datum = mysqli_escape_string($zbirka, $podatki["datum"]);
		$trajanje = mysqli_escape_string($zbirka, $podatki["trajanje"]);
		$agencija = mysqli_escape_string($zbirka, $podatki["agencija"]);
		$cena = mysqli_escape_string($zbirka, $podatki["cena"]);
		$opis_aranzmaja = mysqli_escape_string($zbirka, $podatki["opis_aranzmaja"]);
		
			$poizvedba="INSERT INTO potovanje (IDdestinacije, datum, trajanje, agencija, cena, opis_aranzmaja) VALUES ('$IDdestinacije', '$datum', '$trajanje', '$agencija', '$cena', '$opis_aranzmaja')";
			
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(201);
				$odgovor = URL_vira($IDdestinacije);
				echo json_encode($odgovor);
			}
			else
			{
				http_response_code(500);
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
	}
	else
	{
		http_response_code(405);
	}
}
 
function posodobi_potovanje($IDpotovanja)
{
	global $zbirka, $DEBUG;
 
	$podatki = json_decode(file_get_contents("php://input"),true);

	if(isset($podatki["IDdestinacije"], $podatki["datum"], $podatki["trajanje"], $podatki["agencija"], $podatki["cena"],$podatki["opis_aranzmaja"]))
	{
			$IDdestinacije = mysqli_escape_string($zbirka, $podatki["IDdestinacije"]);
			$datum = mysqli_escape_string($zbirka, $podatki["datum"]);
			$trajanje = mysqli_escape_string($zbirka, $podatki["trajanje"]);
			$agencija = mysqli_escape_string($zbirka, $podatki["agencija"]);
			$cena = mysqli_escape_string($zbirka, $podatki["cena"]);
			$opis_aranzmaja = mysqli_escape_string($zbirka, $podatki["opis_aranzmaja"]);
			
			$poizvedba="UPDATE potovanje SET IDdestinacije='$IDdestinacije', datum='$datum', trajanje='$trajanje', agencija='$agencija', cena='$cena', opis_aranzmaja='$opis_aranzmaja' WHERE IDpotovanja='$IDpotovanja'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
	}
	else
	{
		http_response_code(400);
	}
}
function izbrisi_potovanje($IDpotovanja)
{
	global $zbirka, $DEBUG;
	
	$poizvedba="DELETE from potovanje WHERE IDpotovanja='$IDpotovanja'";
			if(mysqli_query($zbirka, $poizvedba))
			{
				http_response_code(204);
			}
			else
			{
				http_response_code(500);
				
				if($DEBUG)
				{
					pripravi_odgovor_napaka(mysqli_error($zbirka));
				}
			}
}
?>
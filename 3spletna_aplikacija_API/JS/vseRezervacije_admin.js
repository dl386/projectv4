function vseRezervacije_admin()
{
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var podatki = JSON.parse(this.response);
				prikaziPodatke(podatki);
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb", true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}
 
function prikaziPodatke(odgovorJSON){
   var fragment = document.createDocumentFragment();
 
   for (var i = 0; i < odgovorJSON.length; i++) {
		var tr = document.createElement("tr");
 
      for(var stolpec in odgovorJSON[i]){
         var td = document.createElement("td");
         td.innerHTML = odgovorJSON[i][stolpec];
         tr.appendChild(td);
      }
      fragment.appendChild(tr);
   }
   document.getElementById("tabela").appendChild(fragment);
}  
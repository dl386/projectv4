const formToJSON = elements => [].reduce.call(elements, (data, element) => 
{
	if(element.name!="")
	{
		data[element.name] = element.value;
	}
  return data;
}, {});

function dodajPotovanje()
{
	const data = formToJSON(document.getElementById("obrazec").elements);
	
	var JSONdata = JSON.stringify(data, null, "  ");
	
	var xmlhttp = new XMLHttpRequest();
	 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 201)
		{
			document.getElementById("odgovor").innerHTML="Dodajanje je uspelo!";
		}
		if(this.readyState == 4 && this.status != 201)
		{
			document.getElementById("odgovor").innerHTML="Dodajanje ni uspelo: "+this.status;
		}
	};
	 
	xmlhttp.open("POST", "http://localhost/PROJEKT/2spletna_storitev_REST/potovanja", true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}
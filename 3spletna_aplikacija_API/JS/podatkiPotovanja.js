const input = document.getElementById('IDpotovanjaVsi');
const datalist = document.getElementById('options-list');

input.addEventListener('input', (event) => {
  const userInput = event.target.value;

  const options = datalist.getElementsByTagName('option');
  var optionFound = false;
  for (var i = 0; i < options.length; i++) {
    if (options[i].value === userInput) {
      optionFound = true;
      break;
    }
  }
});

window.onload = function(){
	podatkiOVsehPotovanj();
};

function podatkiOVsehPotovanj()
{	
	var datalist = document.getElementById('options-list');
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var opcije = JSON.parse(this.response);
				
				for (var i = 0; i < opcije.length; i++){
					var newOption = document.createElement("option");
					newOption.value = opcije[i]["IDpotovanja"];
					datalist.appendChild(newOption);
				}
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/potovanja", false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}

function podatkiPotovanja()
{
	var IDpotovanja = document.getElementById("obrazec")["IDpotovanjaVsi"].value;
	document.getElementById("odgovor").innerHTML="";
 
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var podatki = JSON.parse(this.response);
				prikaziPodatke(podatki);
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/potovanja/"+IDpotovanja, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}
 
function prikaziPodatke(odgovorJSON)
{
	 var fragment = document.createDocumentFragment();
 
	 for(var stolpec in odgovorJSON){				
         var razpon = document.createElement("razpon");	
         var p = document.createElement("p");
         var input = document.createElement("input");	
		 input.id = stolpec;
		 if(input.id == "IDpotovanja")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "ime_destinacije")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "casovni_zig")
		 {
			input.setAttribute("disabled", true);
		 }
		 
		 p.innerText = stolpec + ": ";
		 input.defaultValue =  odgovorJSON[stolpec];
		 
         razpon.appendChild(p);			
         razpon.appendChild(input);			
 
         fragment.appendChild(razpon);
		 
    }
 
	var posodobi = document.createElement("button");
	 
	posodobi.innerHTML="Posodobi potovanje"
	posodobi.addEventListener("click",function () {
		
	var inputIDpotovanja = document.getElementById("IDpotovanja");
	var inputIDdestinacije = document.getElementById("IDdestinacije");
	var inputDatum = document.getElementById("datum");
	var inputTrajanje = document.getElementById("trajanje");
	var inputAgencija = document.getElementById("agencija");
	var inputCena = document.getElementById("cena");
	var inputOpisAranzmaja = document.getElementById("opis_aranzmaja");
		
	var IDdestinacije = inputIDdestinacije.value;
	var datum = inputDatum.value;
	var trajanje = inputTrajanje.value;
	var agencija = inputAgencija.value;
	var cena = inputCena.value;
	var opis_aranzmaja = inputOpisAranzmaja.value;
			
	posodobiPotovanje(odgovorJSON["IDpotovanja"], IDdestinacije, datum, trajanje, agencija, cena, opis_aranzmaja);
	
	});
	 
	var izbrisi = document.createElement("button");
	
	
	izbrisi.innerHTML="Izbriši potovanje"
	izbrisi.addEventListener("click",function () {
		
	izbrisiPotovanje(odgovorJSON["IDpotovanja"])
	});	
	 
	fragment.appendChild(posodobi);
	fragment.appendChild(izbrisi);
	 
	document.getElementById("odgovor").innerHTML="";
	document.getElementById("odgovor").appendChild(fragment);	 
}

function posodobiPotovanje(IDpotovanja, IDdestinacije, datum, trajanje, agencija, cena, opis_aranzmaja) {
	var data =  {
		"IDdestinacije": IDdestinacije,
		"datum": datum,
		"trajanje": trajanje,
		"agencija": agencija,
		"cena": cena,
		"opis_aranzmaja": opis_aranzmaja
	}
	console.log(JSON.stringify(data))
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV je uspela!";
		}
		if(this.readyState == 4 && this.status != 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV ni uspela: "+this.status;
		}
	};
	
	xmlhttp.open("PUT", "http://localhost/PROJEKT/2spletna_storitev_REST/potovanja/"+IDpotovanja, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}

function izbrisiPotovanje(IDpotovanja) {
	var data =  {
		"IDpotovanja": IDpotovanja
	}
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 204)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE je uspelo!";
			document.getElementById("obrazec")["IDpotovanjaVsi"].value = '';
		}
		if(this.readyState == 4 && this.status != 204)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE ni uspelo: "+this.status;
		}
	};
	
	xmlhttp.open("DELETE", "http://localhost/PROJEKT/2spletna_storitev_REST/potovanja/"+IDpotovanja, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}


const input = document.getElementById('IDrezervacijeVsi');
const datalist = document.getElementById('options-list');

input.addEventListener('input', (event) => {
  const userInput = event.target.value;

  const options = datalist.getElementsByTagName('option');
  var optionFound = false; 
  for (var i = 0; i < options.length; i++) {
    if (options[i].value === userInput) {
      optionFound = true;
      break;
    }
  }
});

window.onload = function(){
	podatkiOVsehRezervacijah();
};

function podatkiOVsehRezervacijah()
{	
	var vzdevek = window.localStorage.getItem("username");
	console.log(vzdevek);
	
	var datalist = document.getElementById('options-list');
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var opcije = JSON.parse(this.response);
				
				for (var i = 0; i < opcije.length; i++){
					var newOption = document.createElement("option");
					newOption.value = opcije[i]["IDrezervacije"];
					datalist.appendChild(newOption);
				}
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
	
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb?vzdevekVse="+vzdevek, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}

function podatkiRezervacije_edit_del() 
{
	var IDrezervacije = document.getElementById("obrazec")["IDrezervacijeVsi"].value;
	document.getElementById("odgovor").innerHTML="";
	
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var podatki = JSON.parse(this.response);
				prikaziPodatke(podatki);
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb?IDrezervacije="+IDrezervacije, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}
 
function prikaziPodatke(odgovorJSON)
{
	 var fragment = document.createDocumentFragment();
 
	 console.log(odgovorJSON);

	 for(var stolpec in odgovorJSON){				
         var razpon = document.createElement("razpon");	
         var p = document.createElement("p");
         var input = document.createElement("input");	
		 input.id = stolpec;
		 if(input.id == "IDrezervacije")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "vzdevek")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "ime_destinacije")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "casovni_zig")
		 {
			input.setAttribute("disabled", true);
		 }
		 else if(input.id == "agencija")
		 {
			input.setAttribute("disabled", true);
		 }
		 
		 p.innerText = stolpec + ": ";
		 input.defaultValue =  odgovorJSON[stolpec];
		 
         razpon.appendChild(p);			
         razpon.appendChild(input);			
 
         fragment.appendChild(razpon);
		 
    }
	var posodobi = document.createElement("button");
	 
	posodobi.innerHTML="Posodobi rezervacijo"
	posodobi.addEventListener("click",function () {
		
	var inputIDrezervacije = document.getElementById("IDrezervacije");
	var inputVzdevek = document.getElementById("vzdevek");
	var inputIDpotovanja = document.getElementById("IDpotovanja");
	var inputCasovniZig = document.getElementById("casovni_zig");
		
	var vzdevek = inputVzdevek.value;
	var IDpotovanja = inputIDpotovanja.value;
	var casovni_zig = inputCasovniZig.value;
			
	posodobiRezervacijo(odgovorJSON["IDrezervacije"], vzdevek, IDpotovanja, casovni_zig);
	});
	 
	var izbrisi = document.createElement("button");
	
	izbrisi.innerHTML="Izbriši rezervacijo"
	izbrisi.addEventListener("click",function () {
		
	izbrisiRezervacijo(odgovorJSON["IDrezervacije"])
	});
	 
	 
	fragment.appendChild(posodobi);
	fragment.appendChild(izbrisi);
	 
	document.getElementById("odgovor").innerHTML="";
	document.getElementById("odgovor").appendChild(fragment);	 
}

function posodobiRezervacijo(IDrezervacije, vzdevek, IDpotovanja, casovni_zig) {
	var data =  {
		"IDrezervacije": IDrezervacije,
		"vzdevek": vzdevek,
		"IDpotovanja": IDpotovanja,
		"casovni_zig": casovni_zig
	}
	console.log(JSON.stringify(data))
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV je uspela!";
		}
		if(this.readyState == 4 && this.status != 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV ni uspela: "+this.status;
		}
	};
	
	xmlhttp.open("PUT", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb?IDrezervacije="+IDrezervacije, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}

function izbrisiRezervacijo(IDrezervacije) {
	var data =  {
		"IDrezervacije": IDrezervacije
	}
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 201)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE ni uspelo!";
			document.getElementById("obrazec")["IDrezervacijeVsi"].value= '';
		}
		if(this.readyState == 4 && this.status != 201)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE je uspelo: "+this.status;
		}
	};
	
	xmlhttp.open("DELETE", "http://localhost/PROJEKT/2spletna_storitev_REST/rezervacije_oseb?IDrezervacije="+IDrezervacije, false);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}
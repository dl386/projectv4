const input = document.getElementById('vzdevekVsi');
const datalist = document.getElementById('options-list');

input.addEventListener('input', (event) => {
  const userInput = event.target.value;

  const options = datalist.getElementsByTagName('option');
  var optionFound = false;
  for (var i = 0; i < options.length; i++) {
    if (options[i].value === userInput) {
      optionFound = true;
      break;
    }
  }
});

window.onload = function(){
	podatkiOVsehOsebah();
};

function podatkiOVsehOsebah()
{	
	var datalist = document.getElementById('options-list');
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var opcije = JSON.parse(this.response);
				
				for (var i = 0; i < opcije.length; i++){
					var newOption = document.createElement("option");
					
					newOption.value = opcije[i]["vzdevek"];
					datalist.appendChild(newOption);
				}
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/osebe", true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}

function podatkiOsebe()
{
	var vzdevek = document.getElementById("obrazec")["vzdevekVsi"].value;
	document.getElementById("odgovor").innerHTML="";
 
	var xmlhttp = new XMLHttpRequest();
 
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			try{
				var podatki = JSON.parse(this.response);
				prikaziPodatke(podatki);
			}
			catch(e){
				console.log("Napaka pri razčlenjevanju podatkov");
			}
		}
		if (this.readyState == 4 && this.status != 200)
		{
			document.getElementById("odgovor").innerHTML="Ni uspelo: "+this.status;
		}
	};
 
	xmlhttp.open("GET", "http://localhost/PROJEKT/2spletna_storitev_REST/osebe/"+vzdevek, true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send();
}
 
function prikaziPodatke(odgovorJSON)
{
	 var fragment = document.createDocumentFragment();
 
	 console.log(odgovorJSON);

	 for(var stolpec in odgovorJSON){				
         var razpon = document.createElement("razpon");	
         var p = document.createElement("p");	
         var input = document.createElement("input");	
		 input.id = stolpec;
		 
		 if(input.id == "vzdevek")
		 {
			input.setAttribute("disabled", true);
		 }
		 
		 p.innerText = stolpec + ": ";
		 input.defaultValue =  odgovorJSON[stolpec];
		 
         razpon.appendChild(p);			
         razpon.appendChild(input);			
 
         fragment.appendChild(razpon);
		 
    }
	
	var posodobi = document.createElement("button");
	
	posodobi.innerHTML="Posodobi uporabnika"
	posodobi.addEventListener("click",function () {
		
	var inputVzdevek = document.getElementById("vzdevek");
	var inputIme = document.getElementById("ime");
	var inputPriimek = document.getElementById("priimek");
	var inputEmail = document.getElementById("email");
	var inputVloga = document.getElementById("vloga");
	
	var ime = inputIme.value;
	var priimek = inputPriimek.value;
	var email = inputEmail.value;
	var vloga = inputVloga.value;
			
	posodobiOsebo(odgovorJSON["vzdevek"], ime, priimek, email, vloga);
	
	});
	 
	var izbrisi = document.createElement("button");
	
	izbrisi.innerHTML="Izbriši uporabnika"
	izbrisi.addEventListener("click",function () {
		
	izbrisiOsebo(odgovorJSON["vzdevek"])
	});
	
	fragment.appendChild(posodobi);
	fragment.appendChild(izbrisi);
	 
	document.getElementById("odgovor").innerHTML="";
	document.getElementById("odgovor").appendChild(fragment);	 

}

function posodobiOsebo(vzdevek, ime, priimek, email, vloga) {
	var data =  {
		"ime": ime,
		"priimek": priimek,
		"email": email,
		"vloga": vloga
	}
	console.log(JSON.stringify(data))
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV je uspela!";
		}
		if(this.readyState == 4 && this.status != 204)
		{
			document.getElementById("odgovor").innerHTML="POSODOBITEV ni uspela: "+this.status;
		}
	};
	
	xmlhttp.open("PUT", "http://localhost/PROJEKT/2spletna_storitev_REST/osebe/"+vzdevek, true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}

function izbrisiOsebo(vzdevek) {
	var data =  {
		"vzdevek": vzdevek
	}
	var xmlhttp = new XMLHttpRequest();
	
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 204)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE uspelo!";
			document.getElementById("obrazec")["vzdevekVsi"].value = '';
		}
		if(this.readyState == 4 && this.status != 204)
		{
			document.getElementById("odgovor").innerHTML="BRISANJE ni uspelo: "+this.status;
		}
	};
	
	xmlhttp.open("DELETE", "http://localhost/PROJEKT/2spletna_storitev_REST/osebe/"+vzdevek, true);
	xmlhttp.setRequestHeader("X-API-Key", window.localStorage.getItem("access_token"));
	xmlhttp.send(JSON.stringify(data));
}